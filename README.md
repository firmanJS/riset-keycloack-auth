##### Instalasi dan setup Keycloak, kong via Docker
<!-- '# docker-compose -f docker-compose-keycloak.yaml up --build -d' -->
```bash
docker-compose -f docker-compose.yaml up --build -d
```

<!-- ##### Instalasi Kong via Docker
- tahapan migrasi database
- setup kong
```bash
docker-compose -f docker-compose.yaml up --build -d
``` -->
#### menjalankan service 
- masuk ke folder service open via terminal
```javascript
$ npm install
$ node data.service.js
$ node user.service.js
```
<!-- - tahapan setting endpoint
```bash
docker-compose -f docker-compose.yaml -f docker-compose.set-upstream.yaml up -d
``` -->

##### Setelah instalasi selesai
- buka di browser http://localhost:8080/auth/ dengan username:`admin` dan password:`admin`
- buka di browser http://localhost:8000/ test kong gateway
- buka di browser http://localhost:8001/ test kong gateway

#### wiki
https://gitlab.com/firmanJS/riset-keycloack-auth/wikis/home